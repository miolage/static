import MasterBox from "./MasterBox";

export default class Box {
    constructor(width, height, padding, boxes, gridAreas) {
        this.width = width;
        this.height = height;
        this.padding = padding;
        this.cursor = 0;
        this.currentDiv = null;
        this.boxes = boxes;
        this.grid_areas = gridAreas;

        this.columns = (this.grid_areas[0].match(/ /g) || []).length + 1;
        this.rows = this.grid_areas.length;

        this.masterbox = new MasterBox(
            (this.width * this.columns) + (this.padding * this.columns),
            (this.height * this.rows) + (this.padding * this.rows),
            this.padding
        );
    }

    getCss() {
        let grids = [];
        for (let box of this.boxes) {
            grids.push(`.${box} { grid-area: ${box}; }`);
        }

        return `
${this.masterbox.getCss()}
${grids.join("\n")}

.master {
  display: grid;
  gap: ${this.padding}px ${this.padding}px;
  grid-template-columns: ${(this.width + "px").repeat(this.columns)};
  grid-template-rows: ${(this.height + "px").repeat(this.rows)};
  grid-template-areas: "${this.grid_areas.join('" "')}";
}`;
    }

    getContainer() {
        let div = document.createElement("div");
        div.id = "grid";
        div.className = "wrapper";
        for (const areaRow of this.masterbox.areas) {
            for (const box of areaRow) {
                div.appendChild(box.domContent);
            }
        }
        return div;
    }

    add (position, callbackUpdate, callbackInsert) {
        if (this.cursor % this.boxes.length === 0) {
            // create master box
            let mainDiv = this.masterbox.add().domContent;

            // fill with sub boxes
            for (let i = 0; i < this.boxes.length; i++) {
                let innerDiv = document.createElement("div");
                innerDiv.dataset.position = this.cursor + i;
                innerDiv.className = this.boxes[i] + " box";

                if (this.boxes[i] === "wide") {
                    innerDiv.style.width = ((this.width * 2) + this.padding) + "px";
                } else {
                    innerDiv.style.width = this.width + "px";
                }
                if (this.boxes[i].startsWith("tower")) {
                    innerDiv.style.height = ((this.height * 2) + this.padding) + "px";
                } else {
                    innerDiv.style.height = this.height + "px";
                }
                mainDiv.appendChild(innerDiv);

                if (this.cursor + i === position) {
                    this.currentDiv = innerDiv;
                    this.currentDiv.onclick = callbackUpdate;
                } else {
                    innerDiv.onclick = callbackInsert;
                }
            }
        } else {
            for (let i=this.cursor; i<=position; i++) {
                if (this.currentDiv.nextSibling) {
                    this.currentDiv = this.currentDiv.nextSibling;
                }
            }
            this.currentDiv.onclick = callbackUpdate;
        }

        // increase cursor
        this.cursor = this.cursor + 1;

        return this.currentDiv;
    }
}