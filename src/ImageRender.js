export default class ImageRender {
    static canUseWebP() {
        let elem = document.createElement('canvas');

        if (!!(elem.getContext && elem.getContext('2d'))) {
            // was able or not to get WebP representation
            return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
        }

        // very old browser like IE 8, canvas not supported
        return false;
    }

    constructor() {
        this.format = ImageRender.canUseWebP() ? "webp" : "jpg";
    }

    getUrl (o, width, height) {
        return "https://media.miolage.de/"+o.collage_id+"/"+o.id+
            "?format="+this.format+"&width="+parseInt(width, 10)+"&height="+parseInt(height, 10)+"&cache=" + o.updated_at;
    }
}