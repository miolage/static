export default class Inlay {

    static showCounter(seconds, div, callback) {

        let counter = document.createElement("div");
        counter.style.position = "absolute";
        counter.style.width = "100%";
        counter.style.padding = 10;
        counter.style.opacity = 0.5;
        counter.style.fontSize = "200%";
        counter.style.textAlign = "center";
        counter.style.zIndex = 10;
        counter.style.backgroundColor = "#000";
        counter.style.color = "#fff";
        counter.innerHTML = seconds;
        div.appendChild(counter);

        let interval = window.setInterval(function () {
            seconds = seconds - 1;
            counter.innerHTML = seconds;

            if (seconds === 0) {
                counter.remove();
                callback();
                clearInterval(interval);
            }
        }, 1000);
    }
}