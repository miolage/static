import MasterBoxContainer from "./MasterBoxContainer";

export default class MasterBox {
    constructor(width, height, padding) {
        this.width = width;
        this.height = height;
        this.padding = padding;

        this.cursor = 0;
        this.areas = [];

        this.columns_left_right = 0;
        this.columns_step = 1;
        this.columns_nextbreak = 2;
        this.rows_left_right = 1;
        this.rows_step = 0;
        this.rows_nextbreak = 1;

        this.divs = [];
    }

    addContainer(position) {
        this.divs[position] = new MasterBoxContainer(position);
        return this.divs[position];
    }

    addColumn() {
        for (let i=0; i<this.areas.length; i++) {
            let content = this.addContainer(this.cursor + i);

            if (this.columns_left_right) {
                this.areas[i].push(content);
            } else {
                this.areas[i].unshift(content);
            }
        }
    }

    addRow() {
        let length = this.cursor === 1 ? 1 : this.areas[this.areas.length - 1].length;
        let contentRow = [];
        for (let i=0; i<length; i++) {
            contentRow.push(this.addContainer(this.cursor + i));
        }

        if (this.rows_left_right) {
            this.areas.push(contentRow);
        } else {
            this.areas.unshift(contentRow);
        }
    }

    add() {
        this.cursor = this.cursor + 1;

        // add column
        if (this.cursor === this.columns_nextbreak) {
            this.columns_step = this.columns_step + 2;
            this.columns_nextbreak = this.cursor + this.columns_step;
            this.columns_left_right = (this.columns_left_right + 1) % 2;
            this.addColumn();
        }

        // add row
        if (this.cursor === this.rows_nextbreak) {
            this.rows_step = this.rows_step + 2;
            this.rows_nextbreak = this.cursor + this.rows_step;
            this.rows_left_right = (this.rows_left_right + 1) % 2;
            this.addRow();
        }

        return this.divs[this.cursor];
    }

    getCss() {
        let repeatsX = this.areas[this.areas.length - 1].length;
        let repeatsY = this.areas.length;

        let areas = [];
        for (const areaRow of this.areas) {
            areas.push(areaRow.map(x => `master${x.content}`).join(" "));
        }

        let masters = [];
        for (let i=1; i<=this.cursor; i++) {
            masters.push(`.master${i} { grid-area: master${i} }`);
        }

        return `
.wrapper {
  display: grid;
  gap: ${this.padding}px ${this.padding}px;
  grid-template-columns: ${(this.width + 'px ').repeat(repeatsX)};
  grid-template-rows: ${(this.height + 'px ').repeat(repeatsY)};
  grid-template-areas: "${areas.join(`" "`)}";
}

.master {
  width: ${this.width}px;
  height: ${this.height}px;
}

${masters.join("\n")}
`;
    }
}