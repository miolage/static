export  default class Camera {
    attachToVideo(video) {

        let mediaConfig = {
            video: {
                width: { ideal: 4096 },
                height: { ideal: 2160 },
                facingMode: 'user'
            }
        };
        var errBack = function(e) {
            console.log('An error has occurred!', e);
            alert("no permission for camera");
        };

        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia(mediaConfig).then(function (stream) {
                video.srcObject = stream;
                video.play();
            }).catch(errBack);
        }
        /* Legacy code below! */
        else if(navigator.getUserMedia) { // Standard
            navigator.getUserMedia(mediaConfig, function(stream) {
                video.src = stream;
                video.play();
            }, errBack);
        } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
            navigator.webkitGetUserMedia(mediaConfig, function(stream){
                video.src = window.webkitURL.createObjectURL(stream);
                video.play();
            }, errBack);
        } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
            navigator.mozGetUserMedia(mediaConfig, function(stream){
                video.src = window.URL.createObjectURL(stream);
                video.play();
            }, errBack);
        } else {
            errBack("none of the solutions");
        }
    }
}