import Network from "./Network";
import Box from "./Box"
import Camera from "./Camera";
import ImageRender from "./ImageRender";
import Inlay from "./Inlay"

export default class App {
    constructor() {
        this.debug = document.location.hash === '#debug';
        this.collage = null;
    }

    addImages (res) {
        var app = this;

        let imageRender = new ImageRender();
        let box = new Box(300, 300, 15, app.collage.boxes, app.collage.gridAreas);

        for (let o of res.objects) {
            let div = box.add(o.position,function () {
                return app.imgClick(this, o);
            }, function () {
                return app.imgClick(this);
            });

            let url = imageRender.getUrl(o, div.style.width, div.style.height);
            div.style.backgroundImage = "url(' " + url + " ')";
        }

        if (app.collage.noLimit) {
            let freeSpots = res.num_results % app.collage.boxes.length;
            if (freeSpots === 0) {
                box.add(0,null, function () {
                    return app.imgClick(this);
                });
            }
        }

        document.querySelector("#grid").replaceWith(box.getContainer());

        let style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = box.getCss();
        document.getElementsByTagName('head')[0].appendChild(style);
    };

    snap (div, video, o = null) {
        var app = this;
        let canvas = document.createElement("canvas");
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;

        let ctx = canvas.getContext('2d');
        ctx.translate(canvas.width, 0);
        ctx.scale(-1, 1);

        if (app.debug) {
            ctx.fillStyle = "blue";
            ctx.font = "bold 16px Arial";
            ctx.fillText("0x0", 0, 0);
            ctx.fillText("Zibri", (canvas.width / 2) - 17, (canvas.height / 2) + 8);
        } else {
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

            // remove live video and place picture
            video.srcObject.getTracks()[0].stop();
        }

        div.removeChild(video);
        div.style.backgroundImage = "url(' " + canvas.toDataURL("image/jpeg") + " ')";

        // place save button
        let buttonSave = document.querySelector("#buttonSave");
        buttonSave.style.display = "block";
        buttonSave.onclick = function () {
            let data = {};
            console.log(o);

            // UPDATE
            if (o) {
                data = {
                    id: o.id,
                    collage_id: o.collage_id,
                    img: canvas.toDataURL("image/jpeg"),
                };
            } else {
                // INSERT
                data = {
                    collage_id: app.collage.id,
                    position: div.dataset.position,
                    img: canvas.toDataURL("image/jpeg"),
                };
            }

            Network.request("picture", function () {
                buttonSave.style.display = "none";
            }, data);

            return false;
        };
    };

    // click on the image starts capturing the live video
    imgClick (div, o = null) {
        var app = this;
        let video = document.createElement("video");

        if (app.debug) {
            video.src = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
            video.autoplay = true;
            video.load();
        } else {
            let c = new Camera();
            c.attachToVideo(video);
        }

        video.addEventListener('loadedmetadata', function(e) {
            let factor = parseInt(div.style.width, 10) / video.videoWidth;
            if (factor < 1) {
                factor = parseInt(div.style.height, 10) / video.videoHeight;
            }

            video.width = video.videoWidth * factor;
            video.height = video.videoHeight * factor;
        });

        Inlay.showCounter(3, div, function () {
            app.snap(div, video, o);
        });

        div.appendChild(video);

        return false;
    };

    load() {
        var app = this;
        let code = document.location.pathname.substring(1);
        let q = JSON.stringify({"filters": [{"name": "code", "op": "=", "val": code}]});

        Network.request("collage/?q=" + q, function (res) {

            if (res.num_results !== 1) {
                alert("error loading collage");
            }

            let defaults = {
                boxes: ["wide", "tower-1", "tower-2", "row2-left", "row2-right"],
                gridAreas: ["wide wide tower-1 tower-2", "row2-left row2-right tower-1 tower-2"],
                noLimit: true
            };
            app.collage = Object.assign({}, defaults, res.objects[0]);

            document.querySelector(".header > p").innerText = app.collage.desc;
            document.querySelector(".header > h1").innerText = app.collage.title;

            let q = JSON.stringify({"filters": [{"name": "collage_id", "op": "=", "val": app.collage.id}], "fields": "id,collage_id,position,updated_at", "order_by": [{"field": "position", "direction": "ASC"}]});
            Network.request("picture/?q=" + q, function (res) {
                return app.addImages(res);
            });
        });
    };
}
