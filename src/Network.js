export default class Network {
    static request(url, callback, post_data) {
        let httpRequest = new XMLHttpRequest();
        if (post_data) {
            const formData = new FormData();
            Object.keys(post_data).forEach(key => formData.append(key, post_data[key]));
            post_data = formData;

            httpRequest.open('POST', 'https://api.miolage.de/' + url, true);
        } else {
            httpRequest.open('GET', 'https://api.miolage.de/' + url, true);
        }
        httpRequest.responseType = 'json';
        // httpRequest.setRequestHeader('Authorization', 'Bearer ' + bearer);
        if (callback) {
            httpRequest.onload = function (req) {
                return function () {
                    callback(req.response);
                };
            }(httpRequest);
        }
        httpRequest.send(post_data);
    };
}